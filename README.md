# Kubernetes Getting Started #

Kubernetes comes out of Google. It is also the primary docker mgmt solution in many environments (including Azure and GCP). Shorthand for Kubernetes is K8s.

Setup GCP K8s cluster.. 

```cli
gcloud container clusters get-credentials cluster-1 --zone us-central1-a --project bright-feat-153913

gcloud container clusters list
```

### Notes ###

* K8s uses Masters and Nodes (minions). Masters manage the environment - run workloads on minions. 
* Masters use etcd to manage cluster
* K8s Manifest files written in YAML
* Kubelet
    * The main Kubernetes agent
    * Registers Node with Cluster
    * Watches apiserver (master)
    * Instantiates pods
    * Reports back to Master
* kube-proxy
    * Kubernetes Networking
    * Pod IP Addresses - All containers share a single IP
    * Load Balances across all pods in a service
* Declaritive Model & Desired State
    * K8s operates on a Declaritive model (YAML or JSON).
    * Containers (especially K8s) is at the extreme edge of Cattle vs. Pets. 
    * You don't pass commands in the manifest file - k8s figures out the details
    * First class citizen in APIServer -- Deployed via API (Restful)
    * Replica Sets (next gen)
    * Self Documenting
    * Spec-Once -> Deploy Many
    * Versioned 
    * Makes Roll-out and Roll-back easier
* In Kubernetes containers ALWAYS run inside pods. 
    * You can run more than one container per Pod
    * Pod is just a sandbox on a node
    * Containers share Pod Environemnt (Network stack, IP, Volumes, IPC, Namespace, etc)
    * Pods give containers a 'tightly coupled' option. 
    * You scale by adding new Pods with containers, NOT by adding containers to existing Pod. 
    * Pods are Atomic - you destroy a pod and ALL containers go with it. And the reverse is true - When you bring a Pod up ALL containers come online at the sametime.
*  Docker Services helps manage the communication
    * You can't depend on Pod IPs 
    * Service maintains a solid/steady IP and DNS for communication very much like a Load-Balancers
    * Labels are important!! 
    * Service LB uses labels to decide what to LB. 
    * Using Labels will allow you to do Blue/Green (canary) deployments and easily roll back.
    * Only sends to healthy Pods
    * Support for Session Affinity
    * Can Point to things outside the Cluster
    * Random LB by default (supports DNS-RR)
    * TCP by Default (UDP supported)

## Install Notes ## 

* Minikube
    * Minikube is what Docker is for Win/Mac
    * Creates a local VM with Master/Node cluster
    * Inside of the VM
        * LocalKube (binary): Runs all the K8s bits (Master and Node)
        * Container Runtime: Rocket or Docker container
    * Outside of VM: $Kubectl: K8s controller (cli)
    * Feels/Acts like Production deployment
    * Requires Virt Enabled CPU
    * Supports Windows, Linux, and Mac OSX
    * Download from [link]https://github.com/kubernetes/minikube/releases
    * Start with this command running on Hyper-V. Requires Docker for Windows be installed first.

```cli
minikube start --vm-driver=hyperv --hyperv-virtual-switch=DockerNAT
```
    * Additional commands for Docker/Hyper-V can be found here https://docs.docker.com/machine/drivers/hyper-v/
    * Minikube dashboard is a great tool!!
```cli
minikube dashboard
```

* GCP
    * GKE (Google Kontainer Engine) uses Kubernetes
    * Runs on GCE (Google Compute Engine)
    * Easy enough to deploy
    * GKE handles the Master/Controls - we are only worried with Nodes.
    
```cli
gcloud container clusters get-credentials cluster-1 --zone us-central1-a --project bright-feat-153913

kubetcl get nodes
```

* AWS
    * Kops vs ECS
    * https://github.com/kubernetes/kops
    * will update later

* Azure
    * will update later

* Manual Install
    * will update later

## Working with Pods ##

Notes:
* Scheduling (Atomic Unit)
    * VMWare = VM
    * Docker = Container
    * Kubernetes = Pod : Little larger than Container but lighter than VM
* Pods contain one or more containers
* Define pod in Manifest file -> send to API for deploy
* Pods get a single IP Address and containers are NATd (uses IPC name-spaces)
* Pods communicate via common pod network and fully routable
* Intra-pod communication happens on the Pod Localhost and NAT'd Port mappings for external communication
* If multiple containers in a Pod - ALL containers must come online before the Pod comes up. 
    * Stays in Pending state until all containers are available!!!
    * If the containers don't start - they go to FAILED state
* Desired State!
    * Replicas always running
    * ReplicationController handles this and deploys the Pod
    * K8s runs background service that makes sure the specified number of replicas is running based on Desired State you defined

Lets create a new manifest file called => pods.yml! Notice the meta-data --> These are your Labels!!

```powershell
new-item pod.yml
```

Add the following content to pod.yml

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: hello-pod
  zone: prod
  version: v1
spec:
  containers:
  - name: hello-ctr
    image: nigelpoulton/pluralsight-docker-ci:latest
    ports:
    - containerPort: 8080
```

Now lets deploy our pod..

```powershell
kubectl create -f .\pod.yml
```

``` powershell
# Verify
> kubectl get pods
NAME        READY     STATUS    RESTARTS   AGE
hello-pod   1/1       Running   0          1m
```

``` powershell
# Describe
> kubectl describe pods
Name:         hello-pod
Namespace:    default
Node:         gke-cluster-1-default-pool-8333bca4-0svk/10.128.0.5
Start Time:   Fri, 25 May 2018 10:11:27 -0400
Labels:       <none>
Annotations:  kubernetes.io/limit-ranger=LimitRanger plugin set: cpu request for container hello-ctr
Status:       Running
IP:           10.8.3.3
Containers:
  hello-ctr:
    Container ID:   docker://d2d2f901e9cb28366eef6f062069f3449895b8685e1aa81557f1d572adb8a806
    Image:          nigelpoulton/pluralsight-docker-ci:latest
    Image ID:       docker-pullable://nigelpoulton/pluralsight-docker-ci@sha256:7a6b0125fe7893e70dc63b2c42ad779e5866c6d2779ceb9b12a28e2c38bd8d3d
    Port:           8080/TCP
    State:          Running
      Started:      Fri, 25 May 2018 10:11:59 -0400
    Ready:          True
    Restart Count:  0
    Requests:
      cpu:        100m
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-4h6l5 (ro)
Conditions:
  Type           Status
  Initialized    True
  Ready          True
  PodScheduled   True
Volumes:
  default-token-4h6l5:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-4h6l5
    Optional:    false
QoS Class:       Burstable
Node-Selectors:  <none>
Tolerations:     node.alpha.kubernetes.io/notReady:NoExecute for 300s
                 node.alpha.kubernetes.io/unreachable:NoExecute for 300s
Events:
  Type    Reason                 Age   From                                               Message
  ----    ------                 ----  ----                                               -------
  Normal  Scheduled              1m    default-scheduler                                  Successfully assigned hello-pod to gke-cluster-1-default-pool-8333bca4-0svk
  Normal  SuccessfulMountVolume  1m    kubelet, gke-cluster-1-default-pool-8333bca4-0svk  MountVolume.SetUp succeeded for volume "default-token-4h6l5"
  Normal  Pulling                1m    kubelet, gke-cluster-1-default-pool-8333bca4-0svk  pulling image "nigelpoulton/pluralsight-docker-ci:latest"
  Normal  Pulled                 41s   kubelet, gke-cluster-1-default-pool-8333bca4-0svk  Successfully pulled image "nigelpoulton/pluralsight-docker-ci:latest"
  Normal  Created                41s   kubelet, gke-cluster-1-default-pool-8333bca4-0svk  Created container
  Normal  Started                41s   kubelet, gke-cluster-1-default-pool-8333bca4-0svk  Started container
```

``` powershell
# Specify Pod by name
> kubectl get pods/hello-pod
NAME        READY     STATUS    RESTARTS   AGE
hello-pod   1/1       Running   0          2m
```

``` powershell
# All Namespaces
> kubectl get pods --all-namespaces
NAMESPACE     NAME                                                  READY     STATUS    RESTARTS   AGE
default       hello-pod                                             1/1       Running   0          2m
kube-system   event-exporter-v0.1.8-599c8775b7-bdnj2                2/2       Running   0          2h
kube-system   fluentd-gcp-v2.0.9-2l9l8                              2/2       Running   0          2h
kube-system   fluentd-gcp-v2.0.9-czztq                              2/2       Running   0          1h
kube-system   fluentd-gcp-v2.0.9-jt28s                              2/2       Running   0          2h
kube-system   fluentd-gcp-v2.0.9-kwhw5                              2/2       Running   0          2h
kube-system   fluentd-gcp-v2.0.9-rgzjq                              2/2       Running   0          1h
kube-system   heapster-v1.4.3-86c6587454-h4n7x                      3/3       Running   0          2h
kube-system   kube-dns-778977457c-kclm6                             3/3       Running   0          2h
kube-system   kube-dns-778977457c-lqqk4                             3/3       Running   0          2h
kube-system   kube-dns-autoscaler-7db47cb9b7-bprrg                  1/1       Running   0          2h
kube-system   kube-proxy-gke-cluster-1-default-pool-8333bca4-0svk   1/1       Running   0          1h
kube-system   kube-proxy-gke-cluster-1-default-pool-8333bca4-fw7q   1/1       Running   0          2h
kube-system   kube-proxy-gke-cluster-1-default-pool-8333bca4-hmv5   1/1       Running   0          1h
kube-system   kube-proxy-gke-cluster-1-default-pool-8333bca4-mld3   1/1       Running   0          2h
kube-system   kube-proxy-gke-cluster-1-default-pool-8333bca4-zxlq   1/1       Running   0          2h
kube-system   kubernetes-dashboard-6bb875b5bc-q4jht                 1/1       Running   0          2h
kube-system   l7-default-backend-6497bcdb4d-brg8n                   1/1       Running   0          2h
```

Now lets step this up and add a larger deployment:

```powershell
new-item ./rc.yml
```

```yaml
apiVersion: v1
kind: ReplicationController
metadata:
  name: hello-rc
spec:
  replicas: 10
  selector:
    app: hello-world
  template:
    metadata:
      labels:
        app: hello-world
    spec:
      containers:
      - name: hello-ctr
        image: nigelpoulton/pluralsight-docker-ci:latest
        ports:
        - containerPort: 8080
```

```powershell
kubectl create -f .\rc.yml

# and verify everything is running
> kubectl get pods
NAME             READY     STATUS    RESTARTS   AGE
hello-pod        1/1       Running   0          27m
hello-rc-2gt66   1/1       Running   0          1m
hello-rc-4fbh2   1/1       Running   0          1m
hello-rc-865r7   1/1       Running   0          1m
hello-rc-cxzxm   1/1       Running   0          1m
hello-rc-hpxct   1/1       Running   0          1m
hello-rc-ktb42   1/1       Running   0          1m
hello-rc-qjtvf   1/1       Running   0          1m
hello-rc-spdz9   1/1       Running   0          1m
hello-rc-vrc4n   1/1       Running   0          1m
hello-rc-xvpqc   1/1       Running   0          1m
```

You can scale it up by chaning the 'replicas' line on the rc.yml.. Go ahead and set it to '1' and run again.

Lets clean up!

```powershell
kubectl delete pods --name=hello-pod
pod "hello-pod" deleted
```

## K8s Service ##

* Services are REST objects K8s API that provide stable access points for Pods
* It acts like an abstraction
* Services are required to access services running in Pods (ie containers)
* Service handles: IP, DNS, and Port NATing (backend communication)
* You can put services behind a Providers Load-Balancer (ie ELB)
* Endpoint: Pod IP addresses
* Labels are used to tie Services and Pods together
* Service Discovery:
    * DNS Based (Best)
    * EnvVars -- Passed in at creation



### Wrapping the hello-rc Replication Controller in a Service - the iterative way ###

```cli
$ kubectl expose rc hello-rc --name=hello-lb --target-port=8080 --type=LoadBalancer

$ kubectl describe svc hello-lb

# OUTPUT
Name:                     hello-lb
Namespace:                default
Labels:                   app=hello-world
Annotations:              <none>
Selector:                 app=hello-world
Type:                     LoadBalancer
IP:                       10.11.253.241
LoadBalancer Ingress:     35.184.251.111
Port:                     <unset>  8080/TCP
TargetPort:               8080/TCP
NodePort:                 <unset>  31910/TCP
Endpoints:                10.8.3.20:8080,10.8.3.21:8080,10.8.3.22:8080 + 7 more...
Session Affinity:         None
External Traffic Policy:  Cluster
Events:
  Type    Reason                Age   From                Message
  ----    ------                ----  ----                -------
  Normal  EnsuringLoadBalancer  3m    service-controller  Ensuring load balancer
  Normal  EnsuredLoadBalancer   2m    service-controller  Ensured load balancer
```

Let's verify the site is now accessible:

```cli
curl 35.184.251.111:8080

# OUTPUT
StatusCode        : 200
StatusDescription : OK
Content           : <html><head><title>Pluralsight Rocks</title><link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"/></head><body><div class="container"><div class="jumbotro...
RawContent        : HTTP/1.1 200 OK
                    Connection: keep-alive
                    Content-Length: 409
                    Content-Type: text/html; charset=utf-8
                    Date: Fri, 25 May 2018 17:52:57 GMT
```


That was cool - but now as Infrastructure as Code -- Let's build our YAML file.. Fist we need to remove the service we created via the CLI:

```cli
kubectl get svc

# OUTPUT
NAME         TYPE           CLUSTER-IP      EXTERNAL-IP      PORT(S)          AGE
hello-lb     LoadBalancer   10.11.253.241   35.184.251.111   8080:31910/TCP   5m
kubernetes   ClusterIP      10.11.240.1     <none>           443/TCP          5h

kubectl delete svc hello-lb

#OUTPUT
service "hello-lb" deleted
```

Next let's create a new file called svc.yml.. Couple of things about this code:
* kind = What your deploying via the API
* Metadata = Give it a name and label (remember how important these are)
* Type
    * ClusterIP = Only available to other Pods
    * NodePode = Expose Service Externally
    * LoadBalancer = External Access via LB
* Ports = What port to expose
* Selector App = must match the pods your targeting -> ```cli kubectl describe pods |grep app ```

```yaml
apiVersion: v1
kind: Service
metadata:
  name: hello-svc
  labels:
    app: hello-world
spec:
  type: LoadBalancer
  ports:
  - port: 8080
    nodePort: 30001
    protocol: TCP
  selector:
    app: hello-world
```

Now we create based on our yaml file: 

```powershell
kubectl create -f .\svc.yml

# Verify
> kubectl get svc
NAME         TYPE           CLUSTER-IP     EXTERNAL-IP      PORT(S)          AGE
hello-svc    LoadBalancer   10.11.246.77   35.184.166.209   8080:30001/TCP   52s
kubernetes   ClusterIP      10.11.240.1    <none>           443/TCP          6h

> curl 35.184.166.209:8080


StatusCode        : 200
StatusDescription : OK
Content           : <html><head><title>Pluralsight Rocks</title><link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"/></head><body><div class="container"><div class="jumbotro...
RawContent        : HTTP/1.1 200 OK
                    Connection: keep-alive
                    Content-Length: 409
                    Content-Type: text/html; charset=utf-8
                    Date: Fri, 25 May 2018 18:10:55 GMT
                    X-Powered-By: Express
```

### The big picture 

FILL THIS IN LATER!!!